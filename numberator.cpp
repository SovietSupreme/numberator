//
// by Joseph E. Sloan joe.sloan@outlook.com
//
#include "numberator.h"

std::list<int> 
numberator::generateNumbersFromPool()
{

	std:vector<int> numbers;
	std::list<int> chosen;

	//
	// create  number pool
	//
	for(int i = 1; i <= 80; i++) numbers.push_back(i);

	//
	// pull 20 of the 80 available randomly from the 'pool'
	//
	while(chosen.size() < 20) 
	{
		int spot = rand() % (numbers.size());
		chosen.push_back(numbers[spot]);
		removeFromPool(&numbers,spot);
	}

	chosen.sort();
	return chosen;
}

void
numberator::generateNumbersFromPoo() {
	for(int i=0; i<SEQ_LENGTH; i++) {
		int pick = 1 + rand() % 80;
		for(int j=0; j<i; j++) {
			if(pick == numbers[j]) {
				i--;
				break;
			}
		}
		numbers[i] = pick;		
	}
}

std::list<int> 
numberator::generateNumbersRaw() {

	bool complete = false;
	int it = 0;
	std::list<int> winningNumbers;

	while(it < 20) {
		int number = 1 + rand() % 80;
		if(!alreadyDrawn(&winningNumbers,number)) {
			winningNumbers.push_back(number);
			it++;
		}
	}
	
	winningNumbers.sort();
	return winningNumbers;
}

bool 
numberator::alreadyDrawn(std::list<int> * winningNumbers, int number) {
	std::list<int>::iterator it;
	
	for(it = winningNumbers->begin(); it != winningNumbers->end(); ++it) {
		if(number == int(*it)) {
			//char buffer[254];
			//sprintf(buffer,"Number: %d already exists, choosing another (randomly)");
			//cout << buffer << endl;
			return true;
		}
	}
	return false;
}

void 
numberator::removeFromPool (std::vector<int> * pool,int spot) {
	std::vector<int>::iterator it = pool->begin();
	int idx = 0;
	while(idx != spot) {
		++it;
		idx++;
	}
	//cout << "Erasing #" << *it << " from pool. (spot #" << spot << ")" << endl; 
	pool->erase(it);
}

void 
numberator::results(std::list<int> * winningNumbers)
{
	std::list<int>::iterator it;
	std::string output;

	for(it = winningNumbers->begin(); it != winningNumbers->end(); ++it)
	{
		//cout << setw(2) << setfill('0') << *it << "-";
		char buffer[10];
		int idx = 0;
		if(*it < 10) {
			sprintf(buffer,"0%d-",*it);
		} else {
			sprintf(buffer,"%d-",*it);
		}
		while(buffer[idx] != '\0') {
			output.push_back(buffer[idx]);
			idx++;
		}

	}
	output = output.substr(0,output.length()-1);
	cout << output.c_str() << endl;
}

void
numberator::printResults() {
}

bool
numberator::sortDesc(int a,int b) 
{ 
	return (a < b); 
}

void
numberator::updateSeed() {
	offset++;
}

void
numberator::storeSeed() {
	cout << "Enter a seed: ";
	cin >> seed;
}

void
numberator::storeAmount() {
	cout << "Enter amount to generate: ";
	cin >> amount;
}

numberator::numberator(int iSeed,int iAmount) {
	//
	// init some things
	//
	seed = iSeed;
	amount = iAmount;

	srand(seed);
	
}

void
numberator::resetSeed(void) {
	srand(seed);
}

numberator::numberator(int iSeed) {
	seed = iSeed;
	storeAmount();
	srand(seed);
}
numberator::numberator() {
	storeSeed();
}

numberator::~numberator() {
	//
	// nothing
	//
}



