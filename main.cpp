
#include "numberator.h"

int main()
{		
	std::list<int> numbers;
	
	numberator * g = new numberator();

	for (int idx = 0;idx<10;idx++) {
		numbers = g->generateNumbersFromPool();
		g->results(&numbers);
	}

	g->resetSeed();

	numbers.clear();

	cout << setw(59) << setfill(' ') << endl;

	for (int idx = 0;idx<10;idx++) {	
		numbers = g->generateNumbersRaw();
		g->results(&numbers);
	}

	delete(g);
	return 0;

}//end main

