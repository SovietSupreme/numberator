//
// by Joseph E. Sloan joe.sloan@outlook.com
//
#include <iostream>
#include <stdio.h>
#include <string>
#include <time.h>
#include <math.h>
#include <iomanip>
#include <stdlib.h>
#include <vector>
#include <list>
#include <algorithm>

using namespace std;

#define SEQ_LENGTH 		20

class numberator {

	public:
		numberator(void); //ask for seed on runtime
		numberator(int); // set seed on object creation
		numberator(int,int); // not 100% implemeted yet
		~numberator();
	
	private:
		bool 	sortDesc(int,int);
		bool 	alreadyDrawn(std::list<int> *,int);
		int 	seed,offset,amount;
		int		numbers[SEQ_LENGTH];

	public:
		std::list<int>		generateNumbersFromPool(void);
		void				generateNumbersFromPoo(void);
		std::list<int> 		generateNumbersRaw(void);
		void				printResults(void);
		void 				results(std::list<int> *);
		void 				removeFromPool(std::vector<int>*,int);
		void				updateSeed(void);
		void				storeSeed(void);
		void				storeAmount(void);
		void				resetSeed(void);
};
